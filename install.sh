#!/bin/bash
cd $HOME

function install {
  for program in "$@"
  do
    if ! [ -x "$(command -v $program)" ]; then
      echo "Error: $program is not installed."
      [ -x "$(command -v pacman)" ] && sudo pacman --noconfirm -S $program
      [ -x "$(command -v apt)" ] && sudo apt install -y $program
      [ -x "$(command -v yum)" ] && sudo yum install -y $program
      [ -x "$(command -v brew)" ] && brew install $program
    fi
  done
}

function createLink {
  case "$(uname -s)" in
    Darwin)
      ln -sfn $1 $2
      ;;

    Linux)
      ln -sTf $1 $2
      ;;
  esac
}

install zsh tmux vim gawk autojump git npm

createLink .dotfiles/tmux/tmux.conf ~/.tmux.conf
createLink .dotfiles/tmux ~/.tmux

createLink .dotfiles/zsh/zshrc ~/.zshrc
createLink .dotfiles/zsh ~/.zsh

createLink .dotfiles/vim ~/.vim
createLink .dotfiles/vim/vimrc ~/.vimrc

[ -d ".dotfiles" ] || git clone https://gitlab.com/othondev/dotfiles.git .dotfiles
[ -d ".tmux/plugins/tpm" ] || git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
[ -d ".zplug" ] || curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh

[ $(echo $SHELL) = '/bin/zsh' ] || chsh -s `which zsh`

echo "Enviroment installed"
