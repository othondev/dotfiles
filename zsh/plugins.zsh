source ~/.zplug/init.zsh

zplug Valodim/zsh-curl-completion
zplug cpitt/zsh-dotenv, as:plugin
zplug jimeh/zsh-peco-history, defer:2
zplug plugins/autojump, from:oh-my-zsh
zplug plugins/fzf, from:oh-my-zsh
zplug plugins/git, from:oh-my-zsh
zplug plugins/gitfast, from:oh-my-zsh
zplug romkatv/powerlevel10k, as:theme, depth:1
zplug zsh-users/zsh-autosuggestions
zplug zsh-users/zsh-completions
zplug zsh-users/zsh-syntax-highlighting, defer:2

if ! zplug check --verbose; then
  printf "Install? [y/N]: "
  if read -q; then
    echo; zplug install
  fi
fi

zplug load
