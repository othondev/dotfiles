alias tmux="TERM=screen-256color-bce tmux"

for a in $HOME/.zsh/aliases/scripts/*.sh
do
  aliasName=$( echo $a | sed 's+.*scripts/++g' | sed 's/.sh//g')
  alias $aliasName="$a"
done

alias gitall='f(){ for i in $(ls -d ./*/.git | sed s,/.git,, ); do cd ${i} && tput setaf 1; echo ${i} && tput sgr0; git ${@} ; cd - > /dev/null ; done   }; f'
alias status='curl -s https://gitlab.com/othondev/scripts/raw/master/online/softfax/status_service.sh | sh'
alias chbranch='gitall fetch && gitall pull --all && gitall stash && gitall checkout origin/development && gitall checkout ${1}'
alias c='clear'
alias editor=vim
