#!/bin/bash

method=$1
stackName=$2
export AWS_PROFILE=$3

if [[ ( "$method" != "create" && "$method" != "update"  ) || -z "$stackName" || -z "$AWS_PROFILE"  ]]
then
    echo "usage: ./stack.sh <create|update> <stackName> <profile> "
      echo "Enabled profiles: $(grep -o '\[.*\]' ~/.aws/credentials | xargs)"
        exit 1
      fi

      [ "$method" == "update"  ] && aws_method="update-stack" || aws_method="create-stack"
      IFS='-' read -ra ADDR <<< "$stackName"
env=${ADDR[0]}
app=${ADDR[1]}

aws cloudformation $aws_method --stack-name ${env}-${app} --template-body file://${PWD}/cloudformation-softfax-$app.yaml --parameters file://${PWD}/infra-structure/parameters/$env/$app.json --capabilities CAPABILITY_IAM
